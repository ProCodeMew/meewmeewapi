# MeewMeew API

[![npm-version](https://img.shields.io/npm/v/meewmeewapi?style=flat-square)](https://www.npmjs.org/package/meewmeewapi) ![GitHub Repo stars](https://img.shields.io/github/stars/ProCoderMew/meewmeewapi?style=social) ![GitHub followers](https://img.shields.io/github/followers/ProCoderMew?style=social)

Simple Javascript Client Library for [MeewMeew](https://meewmeew.info/site) APIs from Node.js

## Installation:

Node.js or other environments using npm:
```bash
npm install --save meewmeewapi
```

## Documentation

You can see it [here](https://github.com/ProCoderMew/meewmeewapi/blob/MeewMeew/DOCS.md).

## Usage Examples:

### Node.js

```js
const { MeewMeew } = require('meewmeewapi');
const meewmeew = new MeewMeew('YOUR_APIKEY_HERE'); // Get your free API key at https://meewmeew.info/site
```